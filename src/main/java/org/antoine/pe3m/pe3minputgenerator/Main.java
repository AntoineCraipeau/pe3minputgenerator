package org.antoine.pe3m.pe3minputgenerator;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Main {

    public static int FILES_TO_GENERATE = 100;
    public static int MAX_ENTRIES_PER_FILE = 100;
    public static int MIN_ENTRIES_PER_FILE = 5;
    public static int MAX_VALUE = 9999;
    public static int MIN_VALUE = 1;
    public static LocalDateTime MAX_DATE = LocalDateTime.now();
    public static LocalDateTime MIN_DATE = LocalDateTime.now().minusYears(20);

    public static String OUTPUT_DIRECTORY =  "src/main/resources/output/";


    public static void main(String[] args) {
        System.out.println("Generating " + FILES_TO_GENERATE + " files ...");
        int totalEntries = 0;
        for (int i = 0; i < FILES_TO_GENERATE; i++) {
            System.out.println("Generating file " + (i + 1) + "/" + FILES_TO_GENERATE);
            totalEntries += generateFile();
        }
        System.out.println("Generation completed : " + FILES_TO_GENERATE + " files generated with a total of " + totalEntries + " entries");
    }

    private static int generateFile() {
        int numberOfEntries = (int) (Math.random() * (MAX_ENTRIES_PER_FILE - MIN_ENTRIES_PER_FILE + 1) + MIN_ENTRIES_PER_FILE);
        List<Entry> entries = generateEntries(numberOfEntries);

        try (CSVWriter writer = new CSVWriter(new FileWriter(OUTPUT_DIRECTORY + generateFileName()), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.RFC4180_LINE_END)){
            writer.writeNext(new String[]{"reference", "value", "emission-date"});
            for (Entry entry : entries) {
                writer.writeNext(new String[]{entry.reference(), String.valueOf(entry.value()), entry.emissionDate().toString()});
            }
        } catch ( IOException e) {
            System.out.println("An error occurred while writing the file : " + e.getMessage());
        }
        return numberOfEntries;
    }

    private static String generateFileName() {
        return UUID.randomUUID() + "-" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss")) + ".csv";
    }

    private static List<Entry> generateEntries(int numberOfEntries) {
        ArrayList<Entry> entries = new ArrayList<>();
        for (int i = 0; i < numberOfEntries; i++) {
            entries.add(generateEntry());
        }
        return entries;
    }

    private static Entry generateEntry() {
        String reference = "PX-FR" + (int) (Math.random() * 95999) + "-" + (int) (Math.random() * 99999);
        long value = (long) (Math.random() * (MAX_VALUE - MIN_VALUE + 1) + MIN_VALUE);
        LocalDateTime emissionDate = LocalDateTime.of(
                (int) (Math.random() * (MAX_DATE.getYear() - MIN_DATE.getYear() + 1) + MIN_DATE.getYear()),
                (int) (Math.random() * 12 + 1),
                (int) (Math.random() * 28 + 1),
                (int) (Math.random() * 24),
                (int) (Math.random() * 60),
                (int) (Math.random() * 60)
        );
        return new Entry(reference, value, emissionDate);
    }
}

record Entry(String reference, long value, LocalDateTime emissionDate) {
}